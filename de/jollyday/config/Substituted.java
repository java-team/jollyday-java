//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Substituted.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Substituted"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ON_SATURDAY"/&gt;
 *     &lt;enumeration value="ON_SUNDAY"/&gt;
 *     &lt;enumeration value="ON_WEEKEND"/&gt;
 *     &lt;enumeration value="ON_TUESDAY"/&gt;
 *     &lt;enumeration value="ON_WEDNESDAY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Substituted")
@XmlEnum
public enum Substituted {

    ON_SATURDAY,
    ON_SUNDAY,
    ON_WEEKEND,
    ON_TUESDAY,
    ON_WEDNESDAY;

    public String value() {
        return name();
    }

    public static Substituted fromValue(String v) {
        return valueOf(v);
    }

}
