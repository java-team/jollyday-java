//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für FixedWeekdayBetweenFixed complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FixedWeekdayBetweenFixed"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.example.org/Holiday}Holiday"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="from" type="{http://www.example.org/Holiday}Fixed"/&gt;
 *         &lt;element name="to" type="{http://www.example.org/Holiday}Fixed"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="weekday" type="{http://www.example.org/Holiday}Weekday" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FixedWeekdayBetweenFixed", propOrder = {
    "from",
    "to"
})
public class FixedWeekdayBetweenFixed
    extends Holiday
{

    @XmlElement(required = true)
    protected Fixed from;
    @XmlElement(required = true)
    protected Fixed to;
    @XmlAttribute(name = "weekday")
    protected Weekday weekday;

    /**
     * Ruft den Wert der from-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Fixed }
     *     
     */
    public Fixed getFrom() {
        return from;
    }

    /**
     * Legt den Wert der from-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Fixed }
     *     
     */
    public void setFrom(Fixed value) {
        this.from = value;
    }

    /**
     * Ruft den Wert der to-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Fixed }
     *     
     */
    public Fixed getTo() {
        return to;
    }

    /**
     * Legt den Wert der to-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Fixed }
     *     
     */
    public void setTo(Fixed value) {
        this.to = value;
    }

    /**
     * Ruft den Wert der weekday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Weekday }
     *     
     */
    public Weekday getWeekday() {
        return weekday;
    }

    /**
     * Legt den Wert der weekday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Weekday }
     *     
     */
    public void setWeekday(Weekday value) {
        this.weekday = value;
    }

}
